/**
 * Gets all translations from localstorage
 *
 *
 */
export const getHistory = () => {
  const storeValue = localStorage.getItem("history");

  if (storeValue) {
    return JSON.parse(storeValue);
  }
  return [];
};

/**
 * Adds all translations to localstorage
 *
 *
 */
export const addHistory = (value) => {
  const history = getHistory();

  if (history.length >= 10) {
    history.shift();
  }

  history.push(value);
  localStorage.setItem("history", JSON.stringify(history));
};

/**
 * Gets username from localstorage
 *
 */
export const getUserName = () => {
  const storeUserName = localStorage.getItem("username");

  if (storeUserName) {
    return JSON.parse(storeUserName);
  }
  return false;
};

/**
 * Sets user from localstorage
 *
 */
export const setUserName = (value) => {
  localStorage.setItem("username", JSON.stringify(value));
};
