import React from "react";
import Header from "./components/header/header";
import Profile from "./components/Profile/index";

import Start from "./components/Start/index";
import Translation from "./components/Translation/Translation";
import { BrowserRouter, Switch, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Header />
        <Switch>
          <Route path="/" exact component={Start} />
          <Route path="/translation" component={Translation} />
          <Route path="/profile" component={Profile} />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
