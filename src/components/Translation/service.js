export const getTranslation = (keywords) => {
  let signSequencePngs = [];

  keywords.forEach((keyword) => {
    for (let letter of keyword) {
      signSequencePngs.push(letter.toLowerCase());
    }
  });
  return signSequencePngs;
};

/**
 * Function lopping our input letters and adding them to array
 *
 */
