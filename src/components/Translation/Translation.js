import React from "react";
import { useState } from "react";
import { updateUser } from "../../api/api";
import { addHistory, getUserName } from "../../utils/storage";
import { notLoggetIn } from "../hoc/withAuth";
import { getTranslation } from "./service";

const Translation = () => {
  const [inputData, setinputData] = useState([]);
  const [png, setPng] = useState([]);

  /**
   * On submit function translate to hand sign and adds translasation input to database.
   * @param event
   */
  const onSubmit = async (event) => {
    event.preventDefault();

    /**
     * Regex if user enter wrong lettets a alert will popup
     */
    if (/^[a-zA-Z ]+$/.test(inputData.toString().replaceAll(",", ""))) {
      const translated = getTranslation(inputData);
      setPng(translated);
      await updateUser(getUserName(), inputData);

      addHistory(inputData);
    } else {
      alert("Enter only english letters please.");
    }
  };

  /**
   * Splitting our values to array
   *
   */
  const handelChange = (event) => {
    const searchQuery = event.target.value.split(/[ ,.-]+/);
    setinputData(searchQuery);
  };

  return (
    <div className="form">
      <form className="form-translate">
        <input
          id="test"
          type="text"
          name="name"
          onChange={handelChange}
          className="translate-input"
          placeholder="Enter words to translate..."
        ></input>
        <button className="btnButton" onClick={onSubmit}>
          Translate
        </button>
      </form>
      <div className="translations-container">
        {png &&
          png.map((item, index) => (
            <img
              key={index}
              src={`./individial_signs/${item}.png`}
              alt="bilder"
            />
          ))}
      </div>
    </div>
  );
};

export default notLoggetIn(Translation);
