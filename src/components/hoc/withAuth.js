import { Redirect } from "react-router-dom";

/**
 * Function Looks if user is logged in or not if not sending user to login page
 *
 * @param {Component}
 *
 */
export function notLoggetIn(Component) {
  return function () {
    if (localStorage.getItem("username") === null) {
      return <Redirect to="/" />;
    } else {
      return <Component />;
    }
  };
}

/**
 *
 * Function if user is logged in render user to translation page
 */
export function loggedIn(Component) {
  return function () {
    if (localStorage.getItem("username") !== null) {
      return <Redirect to="/translation" />;
    } else {
      return <Component />;
    }
  };
}
