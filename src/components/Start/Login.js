import { addUserDB } from "../../api/api";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import { loggedIn } from "../hoc/withAuth";
import { setUserName } from "../../utils/storage";
import SplashLogo from "../../assets/Splash.svg";
import RobotLogo from "../../assets/Logo.png";
/**
 * Function if you enter a blank username you will get a alert
 * @param {username}
 */
const handleLogin = (username) => {
  try {
    if (username !== "") setUserName(username);
    else {
      alert("Name cannot be empty.");
    }
  } catch (e) {
    console.log(e);
  }
};

/**
 * Function sets username and takes it value
 *  Adding user to json-database
 */
const Login = () => {
  const [username, setUser] = useState("");
  const history = useHistory();

  const onInputChange = (e) => {
    setUser(e.target.value);
  };

  /**
   * Adding user to database
   *
   */
  const addUser = (username) => {
    const request = {
      id: 0,
      username: username,
      translation: [],
    };
    addUserDB(request);
  };

  /**
   * Function Async calling handle login and adding user to database from method
   *
   * @param {e}
   * @returns translation page
   */
  const login = async (e) => {
    handleLogin(username);
    addUser(username);
    return history.push("/translation");
  };

  return (
    <div>
      <form className="form">
        <div className="logos">
          <img className="splashlogo" src={SplashLogo} alt="Splash logo" />
          <img className="robotlogo" src={RobotLogo} alt="Robot logo" />
        </div>
        <p className="title">Lost in Translation</p>
        <p className="span-get-started">Get started</p>
        <div className="input">
          <input
            id="username"
            type="text"
            placeholder="What's your name?"
            className="username-input"
            onChange={onInputChange}
          />
          <button onClick={login} className="btn-login">
            Login
          </button>
        </div>
      </form>
    </div>
  );
};

export default loggedIn(Login);
