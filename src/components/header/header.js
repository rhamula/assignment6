import { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { getUserName } from "../../utils/storage";

function Header() {
  const history = useHistory();
  const location = useLocation();
  const [user, setUser] = useState("");

  /**
   * Gets user in realtime when pages loads
   */
  useEffect(() => {
    setUser(getUserName());
  }, [location]);

  /**
   * Handle on click function to render to profile page
   *
   * @returns to profile page
   */
  const handleProfileClick = () => {
    return history.push("/profile");
  };

  /**
   * Handle on click function to render to home page
   *
   * @returns to home page
   */
  const homeClick = () => {
    return history.push("/");
  };

  return (
    <header className="header">
      <p className="header-title" onClick={homeClick}>
        Lost in Translation
      </p>
      <div className="header-right">
        <p onClick={handleProfileClick} className="header-user">
          {user}
        </p>
      </div>
    </header>
  );
}
export default Header;
