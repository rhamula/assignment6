import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import { deletePosts, getUserByName } from "../../api/api";
import { getHistory, getUserName } from "../../utils/storage";
import { notLoggetIn } from "../hoc/withAuth";

const Profile = () => {
  const [show, setShow] = useState(true);

  const t = getUserName();
  /**
   * UseEffect on page load gets user
   */
  useEffect(() => {
    getUserName(t);
  }, []);

  const histories = getHistory();
  const hist = useHistory();

  /**
   * Maps our translations and displaying
   *
   */
  const displayHistory = histories.map((history, id) => (
    <li key={id}> {history}</li>
  ));

  /**
   * Delete Function gets current user and marks all translations to string deleted
   * and removes them from localstorage
   */
  const handleOnClick = async () => {
    const user = await getUserByName(getUserName());
    const translations = user[0].translation;
    const updatedDeletedTranslations = translations.map((r) => "DELETED");
    await deletePosts(getUserName(), updatedDeletedTranslations);
    localStorage.removeItem("history");
    setShow(false);
  };

  /**
   * Logout function when press clearing localstorage
   * and renders user to homepage
   *
   */
  const logOut = () => {
    localStorage.clear();
    return hist.push("/");
  };

  return (
    <div className="profile-container">
      <p className="profile-title">Translations</p>
      <form className="profile-form">
        <div>{show && <p>{displayHistory}</p>}</div>
      </form>
      <div className="btn-profile">
        <button onClick={handleOnClick}>Delete</button>
        <button onClick={logOut}>Logout</button>
      </div>
    </div>
  );
};

export default notLoggetIn(Profile);
