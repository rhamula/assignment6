// export const JSON_SERVER_URL = "http://localhost:4000";
// export const GET_USER_BY_NAME_URL = "http://localhost:4000/users?username=";
export const JSON_SERVER_URL = "https://fake-server-translations.herokuapp.com";
export const GET_USER_BY_NAME_URL =
  "https://fake-server-translations.herokuapp.com/users?username=";
/**
 * Gets a user by name
 * @param {username} username
 * @returns
 */
export async function getUserByName(username) {
  try {
    let response = await fetch(GET_USER_BY_NAME_URL + username);
    let data = await response.json();

    return data;
  } catch (error) {
    console.log(error);
  }
}

/**
 * Adds user to json-database
 * @param {user}
 */
export const addUserDB = (user) => {
  fetch(`${JSON_SERVER_URL}/users/`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(user),
  })
    .then((response) => response.json())
    .then((data) => {
      console.log("Success:", data);
    })
    .catch((error) => {
      console.error("Error:", error);
    });
};

/**
 * Gets a user and adds translation string to translation
 *
 * @param {username}
 * @param {translation}
 */
export async function updateUser(username, translation) {
  let user = await getUserByName(username);

  console.log(user);
  let updateTranslations = user[0].translation;
  updateTranslations.push(translation);
  let updatedUser = {
    username: username,
    translation: updateTranslations,
  };
  try {
    await fetch(`${JSON_SERVER_URL}/users/${user[0].id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(updatedUser),
    });
  } catch (e) {
    console.log(e);
  }
}

/**
 * Gets user and marks all translations wih deleted
 *
 * @param {username}
 * @param {translation }
 */
export async function deletePosts(username, translation) {
  let user = await getUserByName(username);
  let updatedUser = {
    username: username,
    translation: translation,
  };
  try {
    await fetch(`${JSON_SERVER_URL}/users/${user[0].id}`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(updatedUser),
    });
  } catch (e) {
    console.log(e);
  }
}
